git remote add bbucket git@bitbucket.org:tmpteam1/sample-repo.git || true
git branch -d -f deploy || true
git checkout -b deploy || true
rm .gitignore || true
git config --global user.email "semaphore-deploymen@test.com"
git config --global user.name "Deployment Server"
touch tmp.file
git add tmp.file
git commit -m "tmp.file" || true
git push bbucket deploy:master --force
